using System.Net.Http.Headers;
using System.IO.Pipes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Moves : MonoBehaviour
{
    private Animator Anim;
    public float WalkSpeed = 0.15f;
    private bool IsJumping = false;
    private bool CanWalkLeft = true;
    private bool CanWalkRight = true;
    private AnimatorStateInfo Player1Layer0;
    public GameObject Player1;
    public GameObject Opponent;
    private UnityEngine.Vector3 OppPosition;
    public static bool FacingLeft = false;
    public static bool FacingRight = true;
    private AudioSource MyPlayer; 
    public AudioClip LightPunch;
    public AudioClip HeavyPunch;
    public AudioClip LightKick;
    public AudioClip HeavyKick;


    void Start()
    {

        Anim = GetComponentInChildren<Animator>();
        StartCoroutine(FaceRight());
        MyPlayer = GetComponentInChildren<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Listen to the Animator
        Player1Layer0 = Anim.GetCurrentAnimatorStateInfo(0);

        // Cannot exit screen
        UnityEngine.Vector3 ScreenBounds =
            Camera.main.WorldToScreenPoint(this.transform.position);

        // Walking Limit for Right and Left Screen Borders
        if (ScreenBounds.x > Screen.width - 200)
        {
            CanWalkRight = false;
        }
        else if (ScreenBounds.x < 200)
        {
            CanWalkLeft = false;
        }
        else
        {
            CanWalkRight = true; 
            CanWalkLeft = true;
        }

        // Get The Oponnent's Position
        OppPosition = Opponent.transform.position;

        // Facing Left or Right of the Opponent
        if(OppPosition.x > Player1.transform.position.x)
        {
            StartCoroutine(FaceLeft());
        }
        if(OppPosition.x < Player1.transform.position.x)
        {
            StartCoroutine(FaceRight());
        }

        // Moving Left and Right
        if (Player1Layer0.IsTag("Motion"))
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                if (CanWalkRight == true)
                {
                    Anim.SetBool("Forward", true);
                    transform.Translate(WalkSpeed, 0, 0);
                }
            }
            if (Input.GetAxis("Horizontal") < 0)
            {
                if (CanWalkLeft == true)
                {
                    Anim.SetBool("Backward", true);
                    transform.Translate(-WalkSpeed, 0, 0);
                }
            }
        }

        if (Input.GetAxis("Horizontal") == 0)
        {
            Anim.SetBool("Forward", false);
            Anim.SetBool("Backward", false);
        }

        // Crouching and Jumping
        if (Input.GetAxis("Vertical") > 0)
        {
            if (IsJumping == false)
            {
                IsJumping = true;
                Anim.SetTrigger("Jump");
                StartCoroutine(JumpPause());
            }
        }

        if (Input.GetAxis("Vertical") < 0)
        {
            Anim.SetBool("Crouch", true);
        }
        if (Input.GetAxis("Vertical") == 0)
        {
            Anim.SetBool("Crouch", false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {   
        if(other.gameObject.CompareTag("FistLight"))
        {
            Anim.SetTrigger("HeadReact");
            MyPlayer.clip = LightPunch;
            MyPlayer.Play();
        }
        if(other.gameObject.CompareTag("FistHeavy"))
        {
            Anim.SetTrigger("HeavyReact");
            MyPlayer.clip = HeavyPunch;
            MyPlayer.Play();
        }
        if(other.gameObject.CompareTag("KickLight"))
        {
            Anim.SetTrigger("HeadReact");
             MyPlayer.clip = LightKick;
             MyPlayer.Play();
        }
        if(other.gameObject.CompareTag("KickHeavy"))
        {
            Anim.SetTrigger("HeavyReact");
             MyPlayer.clip = HeavyKick;
             MyPlayer.Play();
        }
        
    }

    IEnumerator JumpPause()
    {
        yield return new WaitForSeconds(0.4f);
        IsJumping = false;
    }


    IEnumerator FaceLeft()
    {
        if(FacingLeft == true)
        {
            FacingLeft = false;
            FacingRight = true;
            yield return new WaitForSeconds(0.15f);
            Player1.transform.Rotate(0,180,0);
            Anim.SetLayerWeight(1,0);
        }
    }
    IEnumerator FaceRight()
    {
        if(FacingRight == true)
        {
            FacingRight = false;
            FacingLeft = true;
            yield return new WaitForSeconds(0.15f);
            Player1.transform.Rotate(0,-180,0);
            Anim.SetLayerWeight(1,1);
        }
    }
}
