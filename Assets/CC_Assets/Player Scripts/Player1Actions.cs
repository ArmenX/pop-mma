using System.Net.Http.Headers;
using System.IO.Pipes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Actions : MonoBehaviour
{
    public float JumpSpeed = 4.00f;
    public GameObject Player1;
    private Animator Anim;
    private AnimatorStateInfo Player1Layer0;
    private bool HeavyMoving = false;
    public float PunchSlideAmt = 2.0f;
    private AudioSource MyPlayer;
    public AudioClip PunchWoosh;
    public AudioClip KickWhoosh;

    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        MyPlayer = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Heavy Punch Slide 
        if(HeavyMoving == true)
        {   
            if(Player1Moves.FacingLeft == true)
            {
                Player1.transform.Translate(-PunchSlideAmt * Time.deltaTime, 0, 0);
            }
            if(Player1Moves.FacingRight == true)
            {
                Player1.transform.Translate(PunchSlideAmt * Time.deltaTime, 0, 0);
            }
            
        }

        // Listen to the Animator
        Player1Layer0 = Anim.GetCurrentAnimatorStateInfo(0);

        // Main Attack Button Actions
        if(Player1Layer0.IsTag("Motion"))
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Anim.SetTrigger("LightPunch");
            }
            if (Input.GetButtonDown("Fire2"))
            {
                Anim.SetTrigger("HeavyPunch");
            }
            if (Input.GetButtonDown("Fire3"))
            {
                Anim.SetTrigger("LightKick");
            }
            if (Input.GetButtonDown("Jump"))
            {
                Anim.SetTrigger("HeavyKick");
            }
            if (Input.GetButtonDown("Block"))
            {
                Anim.SetTrigger("BlockOn");
            }

        }

        // Crouching Attack
        if(Player1Layer0.IsTag("Crouching"))
        {   
            if (Input.GetButtonDown("Fire3"))
            {
                Anim.SetTrigger("LightKick");
            }
        }
        // Aerial Moves
        if(Player1Layer0.IsTag("Jumping"))
        {   
            if (Input.GetButtonDown("Jump"))
            {
                Anim.SetTrigger("HeavyKick");
            }
        }
    }

    public void JumpUp()
    {
        Player1.transform.Translate(0, JumpSpeed, 0);
    }
    // When Hitting Heavy Punch Character is moving towards Enemy
    public void HeavyPunchMovement()
    {
        StartCoroutine(PunchSlide());
    }

    public void FlipUp()
    {
        Player1.transform.Translate(0, JumpSpeed, 0);
        Player1.transform.Translate(1.0f, 0, 0);
    }

    public void FlipBack()
    {
        Player1.transform.Translate(0, JumpSpeed, 0);
        Player1.transform.Translate(-1.0f, 0, 0);
    }

    public void PunchWhoosSound()
    {
        MyPlayer.clip = PunchWoosh;
        MyPlayer.Play();
    }
    public void KickWhoosSound()
    {
        MyPlayer.clip = KickWhoosh;
        MyPlayer.Play();
    }

    IEnumerator PunchSlide()
    {   
        HeavyMoving = true;
        yield return new WaitForSeconds(0.4f);
        HeavyMoving = false;
    }
}
